
All the service descriptions of OWLS-TC should be stored 
in the service/1.1 folder of the Web server. Therefore, the users should copy 
the descriptions of each 1.1_x folder in the 1.1 folder. We store them 
separately only for practical reasons.